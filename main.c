/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "string.h"
#include "queue.h"
#include "stdio.h"
#include "global.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;

osThreadId ToggleLed1Handle;
osThreadId ToggleLed2Handle;
osThreadId ToogleLed3Handle;
osThreadId ControlButtonHandle;
osThreadId taskUARTHandle;
osTimerId timerLed1Handle;
osTimerId timerLed2Handle;
osSemaphoreId mySemHandle;
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
void controlLed1(void const * argument);
void controlLed2(void const * argument);
void controlLed3(void const * argument);
void controlButton(void const * argument);
void TaskUART(void const * argument);
void callBackLed1(void const * argument);
void callBackLed2(void const * argument);
xSemaphoreHandle simpleMutex;
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
xQueueHandle queueRX;
typedef struct{
	char* data;
} queueData ;

#define MAX_BUFFER_SIZE 30
uint8_t receive_buffer[ MAX_BUFFER_SIZE ];
uint8_t temp[MAX_BUFFER_SIZE];
uint8_t rxbuf= 0;
uint8_t recieve_buffer= 0;
uint8_t command_length = 0;
uint8_t newline= '\n';
uint8_t enter[]= "\r\n";
uint8_t set[]="READY TO SET LED 3 \r\n";
uint8_t reset[]="READY TO RESET LED 3 \r\n";
uint8_t dequeue[]="Send queue success \r\n";
uint8_t fail[]="Send fail \r\n";
queueData *ptrtostruct;

void HAL_UART_RxCpltCallback( UART_HandleTypeDef *  huart2){
	if( huart2 -> Instance == USART2 ) {
		HAL_UART_Receive_IT(huart2, temp, 1 );
		uart_flag = 1;
	}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  HAL_UART_Receive_IT(&huart2, temp, 1);
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of mySem */
  osSemaphoreDef(mySem);
  mySemHandle = osSemaphoreCreate(osSemaphore(mySem), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  simpleMutex = xSemaphoreCreateMutex();
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of timerLed1 */
  osTimerDef(timerLed1, callBackLed1);
  timerLed1Handle = osTimerCreate(osTimer(timerLed1), osTimerPeriodic, NULL);

  /* definition and creation of timerLed2 */
  osTimerDef(timerLed2, callBackLed2);
  timerLed2Handle = osTimerCreate(osTimer(timerLed2), osTimerPeriodic, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  queueRX = xQueueCreate(10 , sizeof(queueData));
  ptrtostruct = pvPortMalloc(sizeof(queueData));
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of ToggleLed1 */
  osThreadDef(ToggleLed1, controlLed1, osPriorityNormal, 0, 128);
  ToggleLed1Handle = osThreadCreate(osThread(ToggleLed1), NULL);

  /* definition and creation of ToggleLed2 */
  osThreadDef(ToggleLed2, controlLed2, osPriorityNormal, 0, 128);
  ToggleLed2Handle = osThreadCreate(osThread(ToggleLed2), NULL);

  /* definition and creation of ToogleLed3 */
  osThreadDef(ToogleLed3, controlLed3, osPriorityNormal, 0, 128);
  ToogleLed3Handle = osThreadCreate(osThread(ToogleLed3), NULL);

  /* definition and creation of ControlButton */
  osThreadDef(ControlButton, controlButton, osPriorityNormal, 0, 128);
  ControlButtonHandle = osThreadCreate(osThread(ControlButton), NULL);

  /* definition and creation of taskUART */
  osThreadDef(taskUART, TaskUART, osPriorityNormal, 0, 128);
  taskUARTHandle = osThreadCreate(osThread(taskUART), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */
  /* Start scheduler */
  osKernelStart();
  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED1_Pin|LED2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : LED3_Pin */
  GPIO_InitStruct.Pin = LED3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED3_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LED1_Pin LED2_Pin */
  GPIO_InitStruct.Pin = LED1_Pin|LED2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : Button_Pin */
  GPIO_InitStruct.Pin = Button_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(Button_GPIO_Port, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_controlLed1 */
/**
  * @brief  Function implementing the ToggleLed1 thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_controlLed1 */
void controlLed1(void const * argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
//	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, 0);
	osTimerStart(timerLed1Handle, 2000);
  for(;;)
  {

  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_controlLed2 */
/**
* @brief Function implementing the ToggleLed2 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_controlLed2 */
void controlLed2(void const * argument)
{
  /* USER CODE BEGIN controlLed2 */
	osTimerStart(timerLed2Handle, 3000);
  /* Infinite loop */
  for(;;)
  {
  }
  /* USER CODE END controlLed2 */
}

/* USER CODE BEGIN Header_controlLed3 */
/**
* @brief Function implementing the ToogleLed3 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_controlLed3 */
void controlLed3(void const * argument)
{
  /* USER CODE BEGIN controlLed3 */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END controlLed3 */
}

/* USER CODE BEGIN Header_controlButton */
/**
* @brief Function implementing the ControlButton thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_controlButton */
void controlButton(void const * argument)
{
  /* USER CODE BEGIN controlButton */
  /* Infinite loop */
  for(;;)
  {


  }
  /* USER CODE END controlButton */
}

/* USER CODE BEGIN Header_TaskUART */
/**
* @brief Function implementing the taskUART thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_TaskUART */
void TaskUART(void const * argument)
{
  /* Infinite loop */
	queueData *ptrtostruct;
	ptrtostruct = pvPortMalloc(sizeof(queueData));
	ptrtostruct->data = "hello from task UART\r\n";
	if(xQueueSend(queueRX, &ptrtostruct, portMAX_DELAY)){
	}
  for(;;)
  {
	if(uart_flag == 1){
		ptrtostruct->data = "hello from interrupt\r\n";
		if(xQueueSend(queueRX, &ptrtostruct, portMAX_DELAY)){
			HAL_UART_Transmit(&huart2, (uint8_t *) dequeue, strlen((char*) dequeue), 100);
		}
		else {
			HAL_UART_Transmit(&huart2, (uint8_t *) fail, strlen((char*) fail), 100);
		};
		uart_flag = 0;
	}
    queueData *myqueue;
    char* ptr;
    if (xQueueReceive(queueRX, &myqueue, portMAX_DELAY) == pdPASS) {
      ptr = pvPortMalloc(100 * sizeof(char));
      if (ptr != NULL) {
        sprintf(ptr, "%s", myqueue->data);
        HAL_UART_Transmit(&huart2, (uint8_t *)ptr, strlen(ptr), HAL_MAX_DELAY);
        vPortFree(ptr);
      }
    }
    vPortFree(myqueue);
  }
}


/* callBackLed1 function */
void callBackLed1(void const * argument)
{
  /* USER CODE BEGIN callBackLed1 */
	HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
	queueData *ptrtostruct;
	ptrtostruct = pvPortMalloc(sizeof(queueData));
	ptrtostruct->data = "hello from task 1\r\n";
	if(xQueueSend(queueRX, &ptrtostruct, portMAX_DELAY)){
	}

  /* USER CODE END callBackLed1 */
}

/* callBackLed2 function */
void callBackLed2(void const * argument)
{
  /* USER CODE BEGIN callBackLed2 */
	queueData *ptrtostruct;
	ptrtostruct = pvPortMalloc(sizeof(queueData));
	ptrtostruct->data = "hello from task 2\r\n";
	if(xQueueSend(queueRX, &ptrtostruct, portMAX_DELAY)){
	}
  /* USER CODE END callBackLed2 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
